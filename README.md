# Whois?
We're a community of 1,000+ software devs on a mission. We want to make Open Banking a reality in South Africa: by building open source tech with [Investec Programmable Banking](https://www.investec.com/en_za/banking/programmable-banking.html). We meet up every month to share what we learn.

# 🚨 Please Note: 🚨
- We are currently in the process of moving all listed open-source projects to thier new Investec Developer Community home on GitHub --> [Check it out here](https://github.com/Investec-Developer-Community/Community-Projects). 
- Check out the [Community Wiki](https://offerzen.gitbook.io/programmable-banking-community-wiki/home/readme) to stay updated with the latest links